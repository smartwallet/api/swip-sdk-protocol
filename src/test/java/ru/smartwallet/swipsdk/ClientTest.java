package ru.smartwallet.swipsdk;

import io.grpc.Channel;
import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;
import io.grpc.inprocess.InProcessChannelBuilder;
import io.grpc.inprocess.InProcessServerBuilder;
import io.grpc.stub.StreamObserver;
import io.grpc.testing.GrpcCleanupRule;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;

import java.util.logging.Level;
import java.util.logging.Logger;

import static org.mockito.AdditionalAnswers.delegatesTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(JUnit4.class)
public class ClientTest {

    /**
     * A simple client that requests {@link STPServiceGrpc} server.
     */
    static class STPServiceGrpcClient {
        private static final Logger logger = Logger.getLogger(STPServiceGrpcClient.class.getName());

        private final STPServiceGrpc.STPServiceBlockingStub blockingStub;

        /** Construct client for accessing HelloWorld server using the existing channel. */
        public STPServiceGrpcClient(Channel channel) {
            // 'channel' here is a Channel, not a ManagedChannel, so it is not this code's responsibility to
            // shut it down.

            // Passing Channels to code makes code easier to test and makes it easier to reuse Channels.
            blockingStub = STPServiceGrpc.newBlockingStub(channel);
        }

        /** Say hello to server. */
        public void initConn() {
            logger.info("Will try to init conn...");
            ConnectionSetting request = ConnectionSetting.newBuilder()
                    .setIp("127.0.0.1")
                    .build();
            ConnectionStatus response;
            try {
                response = blockingStub.initConnection(request);
            } catch (StatusRuntimeException e) {
                logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
                return;
            }
            logger.info("Code: " + response.getCode());
        }
    }

    /**
     * This rule manages automatic graceful shutdown for the registered servers and channels at the
     * end of test.
     */
    @Rule
    public final GrpcCleanupRule grpcCleanup = new GrpcCleanupRule();

    private final STPServiceGrpc.STPServiceImplBase serviceImpl =
            mock(STPServiceGrpc.STPServiceImplBase.class, delegatesTo(
                    new STPServiceGrpc.STPServiceImplBase() {
                        // By default the client will receive Status.UNIMPLEMENTED for all RPCs.
                        // You might need to implement necessary behaviors for your test here, like this:
                        //
                        // @Override
                        // public void sayHello(HelloRequest request, StreamObserver<HelloReply> respObserver) {
                        //   respObserver.onNext(HelloReply.getDefaultInstance());
                        //   respObserver.onCompleted();
                        // }


                        @Override
                        public void initConnection(ConnectionSetting request, StreamObserver<ConnectionStatus> responseObserver) {
                            responseObserver.onNext(
                                    ConnectionStatus.newBuilder()
                                            .setCode(ConnectionStatusCode.CONNECTION_STATUS_CODE_OK)
                                            .build()
                            );
                            responseObserver.onCompleted();
                        }
                    }));

    private STPServiceGrpcClient client;

    @Before
    public void setUp() throws Exception {
        // Generate a unique in-process server name.
        String serverName = InProcessServerBuilder.generateName();

        // Create a server, add service, start, and register for automatic graceful shutdown.
        grpcCleanup.register(InProcessServerBuilder
                .forName(serverName).directExecutor().addService(serviceImpl).build().start());

        // Create a client channel and register for automatic graceful shutdown.
        ManagedChannel channel = grpcCleanup.register(
                InProcessChannelBuilder.forName(serverName).directExecutor().build());

        // Create a HelloWorldClient using the in-process channel;
        client = new STPServiceGrpcClient(channel);
    }

    /**
     * To test the client, call from the client against the fake server, and verify behaviors or state
     * changes from the server side.
     */
    @Test
    public void shouldDeliverMessageToServer() {
        ArgumentCaptor<ConnectionSetting> requestCaptor = ArgumentCaptor.forClass(ConnectionSetting.class);

        client.initConn();

        verify(serviceImpl)
                .initConnection(requestCaptor.capture(), ArgumentMatchers.any());
        Assert.assertEquals("127.0.0.1", requestCaptor.getValue().getIp());
    }
}